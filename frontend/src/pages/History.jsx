import React from 'react';
import {
    Grid
} from '@material-ui/core';
import GameHistory from '../components/history/GameHistory';
import NavButtons from '../components/navbuttons/NavButtons';

const History = () => {

    return (

        <Grid container>
            
            <Grid item lg={12} md={12} sm={12} xs={12}>
                <NavButtons />
            </Grid>

            <Grid item lg={12} md={12} sm={12} xs={12}>
                <GameHistory />
            </Grid>

        </Grid>

    )

}

export default History;