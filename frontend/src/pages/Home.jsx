import React from 'react';
import {
    Grid
} from '@material-ui/core';
import Welcome from '../components/home/Welcome';
import About from '../components/home/About';
import RegisterLogin from '../components/home/RegisterLogin';

const Home = () => {

    return (

        <Grid container>
            
            <Grid item lg={12} md={12} sm={12} xs={12}>
                <Welcome />
            </Grid>

            <Grid item lg={12} md={12} sm={12} xs={12}>
                <About />
            </Grid>

            <Grid item lg={12} md={12} sm={12} xs={12}>
                <RegisterLogin />
            </Grid>

        </Grid>

    )

}

export default Home;