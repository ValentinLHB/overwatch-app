import React from 'react';
import {
    Grid
} from '@material-ui/core';
import NavButtons from '../components/navbuttons/NavButtons';
import Summary from '../components/dashboard/Summary';
import DonutChart from '../components/dashboard/DonutChart';
import LineChart from '../components/dashboard/LineChart';
import Maps from '../components/dashboard/Maps';
import Heroes from '../components/dashboard/Heroes';

const Dashboard = () => {

    return (

        <Grid container>

            <Grid item lg={12} md={12} sm={12} xs={12}>
                <NavButtons />
            </Grid>

            <Grid item lg={12} md={12} sm={12} xs={12}>
                <Summary />
            </Grid>
            
            <Grid item lg={6} md={6} sm={12} xs={12}>
                <DonutChart />
            </Grid>

            <Grid item lg={6} md={6} sm={12} xs={12}>
                <LineChart />
            </Grid>

            <Grid item lg={12} md={12} sm={12} xs={12}>
                <Maps />
            </Grid>

            <Grid item lg={12} md={12} sm={12} xs={12}>
                <Heroes />
            </Grid>

        </Grid>

    )

}

export default Dashboard;