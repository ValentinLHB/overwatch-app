import React from 'react';
import { useStyles } from '../../components/styles/styles';
import {
    Typography, Grid
} from '@material-ui/core';

const Welcome = () => {

    const classes = useStyles();

    return (
        <Grid container>
            <Grid item lg={12} md={12} sm={12} xs={12}>
                <Typography className={classes.maintitle} align="center">
                    Overwatch Performance Tracker
                </Typography>
            </Grid>
        </Grid>
    )

}

export default Welcome;
