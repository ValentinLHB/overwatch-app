import React from 'react';
import { useStyles } from '../../components/styles/styles';
import {
    Button, Typography, Grid
} from '@material-ui/core';

const RegisterLoginButtons = () => {

    const classes = useStyles();

    return (
        <Grid container>
            <Grid item lg={6} md={6} sm={6} xs={6}>
                <Button className={classes.registerloginbutton}>
                    <Typography className={classes.registerloginbuttontypography}>
                        Register
                    </Typography>
                </Button>
            </Grid>
            <Grid item lg={6} md={6} sm={6} xs={6}>
                <Button className={classes.registerloginbutton}>
                    <Typography className={classes.registerloginbuttontypography}>
                        Log In
                    </Typography>
                </Button>
            </Grid>
        </Grid>
    )

}

export default RegisterLoginButtons;