import React from 'react';
import { useStyles } from '../../components/styles/styles';
import {
    Paper, Typography, Grid
} from '@material-ui/core';
import RegisterLoginButtons from './RegisterLoginButtons';

const RegisterLogin = () => {

    const classes = useStyles();

    return (
        <Paper className={classes.paper}>
            <Grid container>
                <Grid item lg={12} md={12} sm={12} xs={12}>
                    <Typography className={classes.cardtitle}>
                        Register & Login
                    </Typography>
                </Grid>
                <RegisterLoginButtons />
            </Grid>
        </Paper>
    )

}

export default RegisterLogin;