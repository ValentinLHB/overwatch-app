import React from 'react';
import { useStyles } from '../../components/styles/styles';
import {
    Paper, Typography, Grid
} from '@material-ui/core';

const About = () => {

    const classes = useStyles();

    return (
        <Paper className={classes.paper}>
            <Grid container>
                <Grid item>
                    <Typography className={classes.cardtitle}>
                        About this site
                    </Typography>
                </Grid>
            </Grid>
        </Paper>
    )

}

export default About;