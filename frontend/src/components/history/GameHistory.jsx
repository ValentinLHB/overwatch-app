import React from 'react';
import { useStyles } from '../styles/styles';
import {
    Paper, Typography, Grid
} from '@material-ui/core';

const GameHistory = () => {

    const classes = useStyles();

    return (
        <Paper className={classes.paper}>
            <Grid container>
                <Grid item>
                    <Typography className={classes.cardtitle}>
                        Game History
                    </Typography>
                </Grid>
            </Grid>
        </Paper>
    )

}

export default GameHistory;