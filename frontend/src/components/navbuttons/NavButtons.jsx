import React from 'react';
import { useStyles } from '../styles/styles';
import {
    Button, Typography, Grid
} from '@material-ui/core';

const NavButtons = () => {

    const classes = useStyles();

    return (
        <Grid container>

            <Grid item lg={8} md={4} sm={4} xs={0}>

            </Grid>

            <Grid item lg={1} md={2} sm={2} xs={3}>
                <Button className={classes.navbutton}>
                    <Typography className={classes.navbuttontypograghy}>
                        Home
                    </Typography>
                </Button>
            </Grid>

            <Grid item lg={1} md={2} sm={2} xs={3}>
                <Button className={classes.navbutton}>
                    <Typography className={classes.navbuttontypograghy}>
                        Add game
                    </Typography>
                </Button>
            </Grid>
            
            <Grid item lg={1} md={2} sm={2} xs={3}>
                <Button className={classes.navbutton}>
                    <Typography className={classes.navbuttontypograghy}>
                        History
                    </Typography>
                </Button>
            </Grid>

            <Grid item lg={1} md={2} sm={2} xs={3}>
                <Button className={classes.navbutton}>
                    <Typography className={classes.navbuttontypograghy}>
                        Dashboard
                    </Typography>
                </Button>
            </Grid>

        </Grid>

    )

}

export default NavButtons;