import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(theme => ({
    navbutton: {
        justifyContent: 'center',
        width: "95%",
        marginTop: "1em",
        marginBottom: "1em",
        borderSpacing: "2%",
        backgroundColor: theme.palette.info.main
    },
    navbuttontypograghy : {
        fontFamily: 'FuturaBQ-DemiBold',
        fontSize: 14,
        color: theme.palette.info.contrastText,
        "&:hover": {
            color: theme.palette.info.main
        }
    },
    registerloginbutton : {
        justifyContent: 'center',
        marginTop: "1em",
        marginBottom: "1em",
    },
    registerloginbuttontypography: {
        fontFamily: 'FuturaBQ-DemiBold',
        fontSize: 20
    },
    cardtitle: {
        fontFamily: 'BigNoodleToo',
        fontSize: 38,
        marginTop: "1%",
        marginBottom: "1%",
        color: theme.palette.secondary.contrastText
    },
    card: {
        display: 'flex',
        margin: "1%",
        backgroundColor: theme.palette.primary.main,
        transition: '0.5s',
        "&:hover": {
            backgroundColor: theme.palette.secondary.main
        }
    },
    cardactionarea: {
        "&:hover": {
            cursor: "default"
        }
    },
    cardcontent: {
        flex: '1 0 auto'
    },
    carddetails: {
        display: 'flex',
        flexDirection: 'column',
    },
    cardbutton: {

    },
    cardbuttontext: {

    },
    cardbody: {
        fontFamily: 'BigNoodleToo',
        fontSize: 26
    },
    cardmedia: {
        width: 150,
    },
    cardmediamaps: {
        height: 200
    },
    maintitle: {
        fontFamily: 'BigNoodleToo',
        fontSize: 40,
        marginTop: "1em",
        // marginBottom: "1em",
        padding: "1rem",
        color: theme.palette.secondary.main
    },
    paper: {
        margin:  "1rem",
        padding: "1rem",
        backgroundColor: theme.palette.primary.main
    },

}));