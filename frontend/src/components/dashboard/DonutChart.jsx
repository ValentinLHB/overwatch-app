import React from 'react';
import { useStyles } from '../../components/styles/styles';
import {
    Paper, Typography, Grid
} from '@material-ui/core';

const DonutChart = () => {

    const classes = useStyles();

    return (
        <Paper className={classes.paper}>
            <Grid container>
                <Grid item>
                    <Typography className={classes.cardtitle}>
                        Donut Chart
                    </Typography>
                </Grid>
            </Grid>
        </Paper>
    )

}

export default DonutChart;