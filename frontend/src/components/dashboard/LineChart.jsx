import React from 'react';
import { useStyles } from '../../components/styles/styles';
import ReactDOM, { render } from 'react-dom';
import { VictoryArea } from 'victory';

const data = [
    {game: 1, SR: 2733},
    {game: 2, SR: 2759},
    {game: 3, SR: 2743},
    {game: 4, SR: 2773},
    {game: 5, SR: 2799},
    {game: 6, SR: 2817},
    {game: 7, SR: 2839},
    {game: 8, SR: 2818},
    {game: 9, SR: 2797},
    {game: 10, SR: 2826},
]

function LineChart(props) {

    return (
        <div>
            <VictoryArea
            style={{
                data: {
                    strokeWidth: 3, 
                    fillOpacity: 0.4,
                    fill: 'orange',
                    stroke: "orange",
                }
            }}
            domain={{ y: [2600, 2900]}}
            domainPadding={10}
            data={data}
            x={"Game"}
            y={"SR"}
            />
        </div>
    );

}

export default LineChart;