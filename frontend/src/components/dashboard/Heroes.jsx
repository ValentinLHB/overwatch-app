import React from 'react';
import { useStyles } from '../../components/styles/styles';
import TankImg from '../../images/heroes/winston.png';
import DamageImg from '../../images/heroes/tracer.png';
import SupportImg from '../../images/heroes/mercy.png';
import {
    Card, 
    CardActions, 
    CardMedia, 
    CardContent, 
    Typography, 
    Grid,
    Button
} from '@material-ui/core';

const Heroes = () => {

    const classes = useStyles();

    return (
        <Grid container>

            <Grid item lg={12} md={12} sm={12} xs={12}>

                <Typography className={classes.maintitle}>
                    Hero Performance
                </Typography>

            </Grid>

            <Grid item lg={4} md={12} sm={12} xs={12}>
                
                <Card className={classes.card} elevation={6}>
                    <CardContent className={classes.cardcontent}>
                        <Typography className={classes.cardtitle}>
                            Tank
                        </Typography>
                        <Typography className={classes.cardbody}>
                            Winston
                        </Typography>
                        <Typography className={classes.cardbody}>
                            72% Winrate
                        </Typography>
                        <Typography className={classes.cardbody}>
                            33% Pickrate
                        </Typography>
                    </CardContent>
                    <CardActions className={classes.cardbutton}>
                        {/* <Button classname={classes.cardbutton}>
                            <Typography className={classes.cardbuttontext}>
                                Button
                            </Typography>
                        </Button> */}
                    </CardActions>
                    <CardMedia
                    className={classes.cardmedia}
                    image={TankImg}
                    title="Winston" />
                </Card>

            </Grid>

            <Grid item lg={4} md={12} sm={12} xs={12}>
                
                <Card className={classes.card} elevation={6}>
                    <CardContent className={classes.cardcontent}>
                        <Typography className={classes.cardtitle}>
                            Damage
                        </Typography>
                        <Typography className={classes.cardbody}>
                            Tracer
                        </Typography>
                        <Typography className={classes.cardbody}>
                            66% Winrate
                        </Typography>
                        <Typography className={classes.cardbody}>
                            25% Pickrate
                        </Typography>
                    </CardContent>
                    <CardActions className={classes.cardbutton}>
                        {/* <Button classname={classes.cardbutton}>
                            <Typography className={classes.cardbuttontext}>
                                Button
                            </Typography>
                        </Button> */}
                    </CardActions>
                    <CardMedia
                    className={classes.cardmedia}
                    image={DamageImg}
                    title="Tracer" />
                </Card>

            </Grid>

            <Grid item lg={4} md={12} sm={12} xs={12}>

                <Card className={classes.card} elevation={6}>
                    <CardContent className={classes.cardcontent}>
                        <Typography className={classes.cardtitle}>
                            Support
                        </Typography>
                        <Typography className={classes.cardbody}>
                            Mercy
                        </Typography>
                        <Typography className={classes.cardbody}>
                            54% Winrate
                        </Typography>
                        <Typography className={classes.cardbody}>
                            75% Pickrate
                        </Typography>
                    </CardContent>
                    <CardActions className={classes.cardbutton}>
                        {/* <Button classname={classes.cardbutton}>
                            <Typography className={classes.cardbuttontext}>
                                Button
                            </Typography>
                        </Button> */}
                    </CardActions>
                    <CardMedia
                    className={classes.cardmedia}
                    image={SupportImg}
                    title="Mercy" />
                </Card>

            </Grid>

        </Grid>
    )

}

export default Heroes;