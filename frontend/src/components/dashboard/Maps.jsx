import React from 'react';
import AssaultImg from '../../images/maps/temple-of-anubis.jpg';
import ControlImg from '../../images/maps/ilios.jpg';
import EscortImg from '../../images/maps/dorado.jpg';
import HybridImg from '../../images/maps/kings-row.jpg';
import { useStyles } from '../../components/styles/styles';
import {
    Card, 
    CardActionArea,
    CardActions, 
    CardMedia, 
    CardContent, 
    Typography, 
    Grid,
    Button,
    Paper
} from '@material-ui/core';

const Maps = () => {

    const classes = useStyles();

    return (
        <Grid container>

            <Grid container>

                <Grid item lg={12} md={12} sm={12} xs={12}>
                    <Typography className={classes.maintitle}>
                        Map Performance
                    </Typography>
                </Grid>

            </Grid>

            <Grid item lg={3} md={6} sm={12} xs={12}>

                <Card className={classes.card} elevation={6}>
                    <CardActionArea className={classes.cardactionarea}>
                        <CardContent className={classes.cardcontent}>
                            <Typography className={classes.cardtitle}>
                                Assault
                            </Typography>
                            <Typography className={classes.cardbody}>
                                Temple of Anubis
                            </Typography>
                            <Typography className={classes.cardbody}>
                                80% Winrate
                            </Typography>
                        </CardContent>
                        <CardActions className={classes.cardbutton}>
                            {/* <Button classname={classes.cardbutton}>
                                <Typography className={classes.cardbuttontext}>
                                    Button
                                </Typography>
                            </Button> */}
                        </CardActions>
                        <CardMedia
                        className={classes.cardmediamaps}
                        image={AssaultImg}
                        title="Temple of Anubis" />
                    </CardActionArea>
                </Card>

            </Grid>

            <Grid item lg={3} md={6} sm={12} xs={12}>

                <Card className={classes.card} elevation={6}>
                    <CardActionArea className={classes.cardactionarea}>
                        <CardContent className={classes.cardcontent}>
                            <Typography className={classes.cardtitle}>
                                Control
                            </Typography>
                            <Typography className={classes.cardbody}>
                                Ilios
                            </Typography>
                            <Typography className={classes.cardbody}>
                                65% Winrate
                            </Typography>
                        </CardContent>
                        <CardActions className={classes.cardbutton}>
                            {/* <Button classname={classes.cardbutton}>
                                <Typography className={classes.cardbuttontext}>
                                    Button
                                </Typography>
                            </Button> */}
                        </CardActions>
                        <CardMedia
                        className={classes.cardmediamaps}
                        image={ControlImg}
                        title="Ilios" />
                    </CardActionArea>
                </Card>

            </Grid>

            <Grid item lg={3} md={6} sm={12} xs={12}>

                <Card className={classes.card} elevation={6}>
                    <CardActionArea className={classes.cardactionarea}>
                        <CardContent className={classes.cardcontent}>
                            <Typography className={classes.cardtitle}>
                                Escort
                            </Typography>
                            <Typography className={classes.cardbody}>
                                Dorado
                            </Typography>
                            <Typography className={classes.cardbody}>
                                75% Winrate
                            </Typography>
                        </CardContent>
                        <CardActions className={classes.cardbutton}>
                            {/* <Button classname={classes.cardbutton}>
                                <Typography className={classes.cardbuttontext}>
                                    Button
                                </Typography>
                            </Button> */}
                        </CardActions>
                        <CardMedia
                        className={classes.cardmediamaps}
                        image={EscortImg}
                        title="Dorado" />
                    </CardActionArea>
                </Card>

            </Grid>

            <Grid item lg={3} md={6} sm={12} xs={12}>

                <Card className={classes.card} elevation={6}>
                    <CardActionArea className={classes.cardactionarea}>
                        <CardContent className={classes.cardcontent}>
                            <Typography className={classes.cardtitle}>
                                Hybrid
                            </Typography>
                            <Typography className={classes.cardbody}>
                                King's Row
                            </Typography>
                            <Typography className={classes.cardbody}>
                                50% Winrate
                            </Typography>
                        </CardContent>
                        <CardActions className={classes.cardbutton}>
                            {/* <Button classname={classes.cardbutton}>
                                <Typography className={classes.cardbuttontext}>
                                    Button
                                </Typography>
                            </Button> */}
                        </CardActions>
                        <CardMedia
                        className={classes.cardmediamaps}
                        image={HybridImg}
                        title="King's Row" />
                    </CardActionArea>
                </Card>

            </Grid>

        </Grid>
    )

}

export default Maps;