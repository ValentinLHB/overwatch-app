import logo from './logo.svg';
import {
    BrowserRouter as Router, Route, Switch
} from 'react-router-dom';
import './App.css';
import React from 'react';
import {
    ThemeProvider,
    Grid
} from '@material-ui/core';
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import History from './pages/History';
import theme from './theme';

function App() {
    return (
        <ThemeProvider theme={theme}>
            <div>
                <Grid item>
                    <Router>
                        <Switch>
                            <Route exact path='/' component={Home} />
                            <Route exact path='/dashboard' component={Dashboard} />
                            <Route exact path='/gamehistory' component={History} />
                        </Switch>
                    </Router>
                </Grid>              
            </div>
        </ThemeProvider>
    )
}

export default App;
