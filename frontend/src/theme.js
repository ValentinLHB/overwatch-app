import { createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
    palette:  {
        primary: {
            main: "#f2efef",
        },
        secondary: {
            main: "#f06514",          
            contrastText: "#000000"
        },
        info: {
            main: "#51b4ff",
            dark: "#3d81ff",
            contrastText: "#ffffff"
        }
    },
    typography: {
        fontFamily: [
            'Roboto',
            'sans-serif'
        ].join(','),
    },
});

export default theme;